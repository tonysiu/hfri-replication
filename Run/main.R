# This script loads all the required libraries and functions to run DLM
# This script should be called prior to runDLM.R, which triggers the actual calculations

require("xts")
require("svSocket")
require("rmongodb")
require("quantmod")
require("PerformanceAnalytics")
require("lubridate")
require("ggplot2")
require("R.utils")
require("dlm")
require("memoise")
require("foreach")
#require("doParallel")
require("doSNOW")
require("plyr")
require("knitr")

# First script to source is srcToEnv.R, which contains the definition of .populate.env()
if( all(regexpr("user:srcToEnv",search())<0) ) {
    attach(NULL,name="user:srcToEnv",pos=2)
    sys.source("./Sandbox/srcToEnv.R",pos.to.env(2))
}

# Clear out old model data if exists
if(!all(regexpr("user:rawData",search())<0)) rm(list=ls("user:rawData", all=T), envir=.name.to.env("user:rawData"))
if(!all(regexpr("user:modelenv",search())<0)) rm(list=ls("user:modelenv", all=T), envir=.name.to.env("user:modelenv"))

.populate.env("user:Sandbox", "./Sandbox/rmongodb.R")
.populate.env("user:Data", "./Data/loaddata.R")
.populate.env("user:fun.dlm", "./Source/fun.dlm/") # need to source BEFORE fun.analysis
.populate.env("user:fun.analysis", "./Source/fun.analysis/")
.populate.env("user:dlm", "./Source/dlm/")
