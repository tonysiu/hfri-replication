# Apply DLM to data
# Returned result is an object of class dlmFiltered
# obj.dlmFiltered in used in unit tests - needs to exist in .GlobalEnv

cat("Running dynamic linear model>>>>>>>>>>>>>>>>>\n\n")
print(system.time(set.global("obj.dlmFiltered", dlm.run("dlm.specs.final"))))
