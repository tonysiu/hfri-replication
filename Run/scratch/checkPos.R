checkPos <- function(endDt) {
    
    dt <- Sys.Date()
    yr.cur <- year(dt)

    #def.par <- par(no.readonly = TRUE) 
    #par(def.par)
    
    pos.temp <- lapply(xts.pos.data.historical, function(x) x[paste("1990-04::", endDt, sep="")])
    pos.df <- data.frame(pos.temp)
#     pos.net <- as.xts(apply(pos.df[,1:7], 1, function(x) {sum(x[1:4], -x[5], x[6:7])}))
#     pos.gross <- as.xts(apply(pos.df[,1:7], 1, function(x) sum(abs(x))))
#     pos.net <- as.xts(apply(pos.df[,1:7], 1, function(x) {sum(x[-5])}))
#     pos.gross <- as.xts(apply(pos.df[,1:7], 1, function(x) sum(abs(x[-5]))))
    pos.net <- as.xts(apply(pos.df[,1:7], 1, function(x) {sum(x)}))
    pos.net.exswap <- as.xts(apply(pos.df[,1:7], 1, function(x) {x[5] <- 0; sum(x)}))
    pos.net.exswaplibor <- as.xts(apply(pos.df[,1:6], 1, function(x) {x[5] <- 0; sum(x)}))
    pos.gross <- as.xts(apply(pos.df[,1:7], 1, function(x) sum(abs(x))))
    pos.long <- as.xts(apply(pos.df[,1:7], 1, function(x) {x <- pmax(x, 0); sum(x)}))
    pos.short <- as.xts(apply(pos.df[,1:7], 1, function(x) {x[5] <- -abs(x[5]); x <- pmin(x, 0); sum(x)})) # putwrite always has a short-component; either short t-bill or short put option
    
    #plot(pos.gross["1990-04::"])
    
    cat("***************************** Net and Gross Positions *****************************\n")

    layout(matrix(seq(1, 6, by=1), byrow = T, ncol = 1))
    op <- par(mar = c(3, 3, 3, 3))
    
    plot(pos.net, main = "Net Position")
    plot(pos.net.exswap, main = "Net Position ex. swaps")
    plot(pos.net.exswaplibor, main = "Net Position ex. swaps and libor")
    plot(pos.gross, main = "Gross Position")
    plot(pos.long, main = "Long Position")
    plot(pos.short, main = "Short Position")
    
    par(op)
}
