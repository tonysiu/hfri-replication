require("svSocket")
require("xts")

con <- socketConnection(host = "Bloomberg-02.corp.seic.com", port = 8888, blocking = FALSE)

getblp<-function(security.identifiers="SPX Index", fields=c("PX_LAST", "CHG_PCT_1D"), start.date=Sys.Date()-366, end.date=Sys.Date(), periodicity="MONTHLY", currency="") {
	retLs<-list()
	for (security.id in security.identifiers) {
		evalServer(con, security, security.id)	# can only take one security, so just take the first one
		evalServer(con, var, fields)
		evalServer(con, start.date, as.POSIXct(start.date))
		evalServer(con, end.date, as.POSIXct(end.date))
		
		if (currency=="") {
			evalServer(con, option.names, c("nonTradingDayFillOption", "nonTradingDayFillMethod", "periodicitySelection"))
			#evalServer(con, option.values, c("ALL_CALENDAR_DAYS", "PREVIOUS_VALUE", periodicity))
			evalServer(con, option.values, c("ALL_CALENDAR_DAYS", "NIL_VALUE", periodicity))
		} else {
			evalServer(con, option.names, c("nonTradingDayFillOption", "nonTradingDayFillMethod", "periodicitySelection", "currency"))
			#evalServer(con, option.values, c("ALL_CALENDAR_DAYS", "PREVIOUS_VALUE", periodicity, currency))
			evalServer(con, option.values, c("ALL_CALENDAR_DAYS", "NIL_VALUE", periodicity, currency))
		}

			evalServer(con, data<-bdh(bconn, security, var, start.date, end.date, always.display.tickers=T, 
			dates.as.row.names=F, include.non.trading.days=T, 
			option_names=option.names, option_values=option.values))
		evalServer(con, dtFormat, "%Y-%m-%d")
		evalServer(con, data.xts<-xts(data[var], order.by=as.Date(data$date, dtFormat), ticker=head(data[,1],1)))
		retLs[gsub(" ", ".", security.id)] <- list(evalServer(con, data.xts))
	}
	if (length(security.identifiers)==1) return(retLs[[1]]) else return(retLs)
}

#samples
#SPTR<-getblp("SPTR Index", "CHG_PCT_1D", "2008-12-31", "2012-03-31", "MONTHLY")
#SPTR<-getblp("SPTR Index", "PX_LAST", "2008-12-31", "2012-03-31", "DAILY")
#SPTR<-getblp("SPTR Index", c("PX_LAST", "CHG_PCT_1D"), "2008-12-31", "2012-03-31", "MONTHLY")
#SPX<-getblp("SPX Index", c("PX_LAST", "CHG_PCT_1D"), "2008-12-31", "2012-03-31", "MONTHLY")

# get DAA returns
#sptr<-getblp("SPTR Index", "PX_LAST", "2012-03-31", "2012-04-20", "DAILY")
#bxiius10<-getblp("BXIIUS10 Index", "PX_LAST", "2012-03-31", "2012-04-20", "DAILY")
#sdlax<-getblp("SDLAX US Equity", "PX_LAST", "2012-03-31", "2012-04-20", "DAILY")
#sgmuksi<-getblp("SGMUKSI ID Equity", "PX_LAST", "2012-03-31", "2012-04-20", "DAILY")
#lapply(list(sptr, bxiius10, sdlax),function(x){periodReturn(x, period="monthly")})
#lapply(list(sptr, bxiius10, sdlax),function(x){periodReturn(x, period="weekly")})

#daa<-getblp(c("SPTR Index", "BXIIUS10 Index", "SDLAX US Equity", "SGMUKSI ID Equity"), "PX_LAST", "2012-03-31", "2012-04-20", "DAILY")
#lapply(daa,function(x){periodReturn(x, period="monthly")})
