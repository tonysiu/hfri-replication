# Set namespace for raw data
.dataNameSpace <- "user:rawData"

# Load raw data
.loadData <- function () {
	if( all(regexpr(.dataNameSpace, search())<0) ) {
		attach(NULL, name = .dataNameSpace, pos=(grep(attr(environment(.loadData), "name"),
												search())
												+ 1))
	}
	load.all.data(mongo, .dataNameSpace)
}

# Check whether data has been loaded
.isDataLoaded <- function() {
	return(any(regexpr(.dataNameSpace,search())>0) && length(ls(.dataNameSpace)) != 0)
}

# Return environment referred by .dataNameSpace if it exists, else
# run .loadData() before returning environment referred by .dataNameSpace 
.rawData <- function () {
	if(.isDataLoaded()) {
		return(.name.to.env(.dataNameSpace))
	} else {
	    cat("\nLoading data from database...\n")
	    
		print(system.time(.loadData()))
        
	    cat("\n\nCalculating factor loadings...\n")
	    
	    return(.name.to.env(.dataNameSpace))
	}
}

# Drop first record of all individual time series in a list
.dropFirst.xtsList <- function (x) {
	x.new <- lapply(x, 
					function(x) {y <-x [-1,]; 
								attributes(y)$ticker <- attributes(x)$ticker; 
								attributes(y)$desname <- attributes(x)$desname; 
								y})
	return(x.new)
}

.dropFirstnMirrorExt.xtsList <- function (x, cutoff="2012-08-31") {
    ## Use on monthly series ONLY (Don't use it on daily series!!!)
    #Drop first record since 1990-01-31 is the first month for which all series have data
	x <- .dropFirst.xtsList(x)
	# Drop first two records since 1990-02-28 is the first month for which all series have data
	#x <- .dropFirst.xtsList(x)
    
    cutoff <- as.Date(cutoff)
    
    split.series <- function (s) {
        if (end(s) > cutoff) {
            s.firstChunk <- s[paste("::", cutoff, sep="")]
            s.secondChunk <- s[paste(cutoff + 1, "::", sep="")]
        } else {
            s.firstChunk <- s
            s.secondChunk <- NULL
        } 
        return (list(s.firstChunk, s.secondChunk))
    }
        
	x.new <- lapply(x, 
					function(x) {
                        temp <- split.series(x)
                        y <- rbind(xts.mirrorext.monthly(temp[[1]]), temp[[2]])
						attributes(y)$ticker <- attributes(x)$ticker; 
						attributes(y)$desname <- attributes(x)$desname; 
						y})
	return(x.new)
}
	
# Calculate rolling 8-week pct change of US M1 money supply
.calc.m1.chg.r8w <- function() {
	m1.chg.weekly <- calc.rnd(na.omit(usm1.daily$CHG_PCT_1D), 8)
	attributes(m1.chg.weekly)$ticker = "M1.chg.weekly"
	attributes(m1.chg.weekly)$desname = "M1.chg.weekly"
	
	m1.chg.monthly <- m1.chg.weekly[endpoints(m1.chg.weekly, on="months")][paste("::",end(ru3000tr$CHG_PCT_1D), sep="")]
	attributes(m1.chg.monthly)$ticker = "M1.chg.monthly"
	attributes(m1.chg.monthly)$desname = "M1.chg.monthly"

	m1.chg.daily <- na.locf(merge(ru3000tr.daily$CHG_PCT_1D, m1.chg.weekly[paste(start(ru3000tr.daily$CHG_PCT_1D),"::",end(ru3000tr.daily$CHG_PCT_1D), sep="")]))
	attributes(m1.chg.daily)$ticker = "M1.chg.daily"
	attributes(m1.chg.daily)$desname = "M1.chg.daily"

	return (list(daily = m1.chg.daily, weekly = m1.chg.weekly, monthly = m1.chg.monthly))
}
	
# Prepare data for models
.prepMonthlyFactorData <- function() {
	# riskfree rate is monthly yield from 3M T-Bill
	riskfree <- h15t3m$PX_LAST/12
	
	# rolling 8-week pct change of US M1 money supply
	m1.chg.monthly <- .calc.m1.chg.r8w()$monthly

	# List of monthly index returns
	# Included indices are: HFRI Equity Hedge (Total) index, monthy risk-free rate from 3M T-Bill,
	# Russell 1000 Total Return, small cap-large cap spread, value-growth spread,
	# S&P 500 put option return, and emerging market-S&P500 Total Return spread
    
# 	dlist.indexvar <- list(hfriehi$CHG_PCT_1D, riskfree, ru1000tr$CHG_PCT_1D, 
# 		sml$CHG_PCT_1D, vmg$CHG_PCT_1D, cboeput$CHG_PCT_1D, emspread$CHG_PCT_1D)
# 	dlist.indexvar <- list(hfriehi$CHG_PCT_1D, riskfree, ru1000tr$CHG_PCT_1D, 
# 	                       sml$CHG_PCT_1D, vmg$CHG_PCT_1D, sp500opt.buywrite$CHG_PCT_1D, emspread$CHG_PCT_1D)
  dlist.indexvar <- list(hfriehi$CHG_PCT_1D, riskfree, ru1000tr$CHG_PCT_1D, 
	                       sml$CHG_PCT_1D, vmg$CHG_PCT_1D, sp500opt$CHG_PCT_1D, emspread$CHG_PCT_1D)
	
	# List of monthly conditioning variable returns
	# Included are Russell 3000 Total Return, risk-free rate from 3M T-Bill, CBOE Vix and CBOE Skew
	dlist.econvar <- list(ru3000tr$CHG_PCT_1D, riskfree, pmax(log(vix$PX_LAST/10),0), skew$PX_LAST/100 - 1, m1.chg.monthly)
	#dlist.econvar <- list(ru3000tr$CHG_PCT_1D, riskfree, vix.price.sd, skew$PX_LAST/100 - 1, m1.chg.monthly)
	#dlist.econvar <- list(ru3000tr$CHG_PCT_1D, riskfree, vix.sd.delt, skew$PX_LAST/100 - 1, m1.chg.monthly)
	
	# List of intercept vectors used in conditioning the beta factors
	dlist.intercept <- list(ru3000tr$CHG_PCT_1D*0+1,ru3000tr$CHG_PCT_1D*0+1)
	#dlist.intercept <- list(ru3000tr$CHG_PCT_1D*0,ru3000tr$CHG_PCT_1D*0)
    
	swapcost <- (us0003m$PX_LAST + 1.75) / 12
	dlist.extra <- list(swapcost)
	
	# Drop end of Dec 1989 data point as not all series have data
	# Extend series into the past to augment training data by mirroring available data thru 2012-08-31
	dlist.indexvar <- .dropFirstnMirrorExt.xtsList(dlist.indexvar, cutoff="2012-08-31")
	dlist.econvar <- .dropFirstnMirrorExt.xtsList(dlist.econvar, cutoff="2012-08-31")
	dlist.intercept <- .dropFirstnMirrorExt.xtsList(dlist.intercept, cutoff="2012-08-31")
	dlist.extra <- .dropFirstnMirrorExt.xtsList(dlist.extra, cutoff="2012-08-31")
	
	# Return combined list of list of monthly data
	#return(c(dlist.indexvar,dlist.econvar,dlist.intercept))
	return(c(dlist.indexvar,dlist.econvar,dlist.intercept,dlist.extra))
}
	
.prepDailyFactorData <- function() {
	# riskfree rate is monthly yield from 3M T-Bill
	riskfree.daily <- h15t3m.daily$PX_LAST/12

	# List of daily index returns
	# Included are daily versions of the monthly factors

# 	dlist.factors.daily <- list(riskfree.daily, ru1000tr.daily$CHG_PCT_1D, sml.daily$CHG_PCT_1D, 
#                                 vmg.daily$CHG_PCT_1D, cboeput.daily$CHG_PCT_1D, emspread.daily$CHG_PCT_1D)
# 	dlist.factors.daily <- list(riskfree.daily, ru1000tr.daily$CHG_PCT_1D, sml.daily$CHG_PCT_1D, 
# 	                            vmg.daily$CHG_PCT_1D, sp500opt.buywrite.daily$CHG_PCT_1D, emspread.daily$CHG_PCT_1D)
	dlist.factors.daily <- list(riskfree.daily, ru1000tr.daily$CHG_PCT_1D, sml.daily$CHG_PCT_1D, 
	                            vmg.daily$CHG_PCT_1D, sp500opt.daily$CHG_PCT_1D, emspread.daily$CHG_PCT_1D)
	
	# Return a list of daily data
	return(dlist.factors.daily)
}	

.prepDailyEconVarData <- function() {
	# rolling 8-week pct change of US M1 money supply
	m1.chg.daily <- .calc.m1.chg.r8w()$daily

	# List of daily conditioning variable returns
	# Included are daily versions of the monthly factors
	dlist.econvar.daily <- list(ru3000tr.daily$CHG_PCT_1D, riskfree.daily, 
						        vix.daily$PX_LAST/100, skew.daily$PX_LAST/100 - 1, m1.chg.daily)

	# Return a list of daily data
	return(dlist.econvar.daily)
}

.scrubMonthlyData <- function() {
	# Get a list of all monthly data variables
	dlist <- .prepMonthlyFactorData()
	
	# Change the year/month/day index to a year/month to resolve 
	# EoM date inconsistencies across variables
	dlist.ym <- dlist
	for(i in 1:length(dlist)) {
		dlist.ym[[i]] <- xts.ymd.to.yearmon(dlist[[i]])
	}
	
	# Get variable names and descriptions from xts attributes
	varnames <- sapply(dlist,function(x) attributes(x)$ticker)
	#varnames[(length(varnames)-1):length(varnames)] <- c("Constant","Constant")
	varnames[(length(varnames)-2):length(varnames)] <- c("Constant","Constant","SwapCost")
	desnames <- sapply(dlist,function(x) attributes(x)$desname)
	#desnames[(length(desnames)-1):length(desnames)] <- c("Intercept.Market","Intercept.Put")
	desnames[(length(desnames)-2):length(desnames)] <- c("Intercept.Market","Intercept.Put","SwapCost")
	
	# Flatten the list of list to a simple list by calling "cbind"
	dlist.scrubbed <- do.call("cbind", dlist.ym)
	colnames(dlist.scrubbed) <- desnames
	dlist.scrubbed[is.na(dlist.scrubbed)] <- 0
	
	return(dlist.scrubbed)
}

######################################################################
# Daily conditioning variables are set to lag 1 time period (1 day)
# Market variables from prior close are used to condition factor
# loadings at beginning of current day
######################################################################
formulate.dlist.model <- function() {
	# Load up all raw data
	.rawData()
	
	# Get scrubbed monthly data
	dlist.scrubbed <- .scrubMonthlyData()

	# Now that the scrubbed data has no NA's and have a year-month time index, we can do
	# arithmetics with the variables
    
	# Use nominal returns instead of pure risky returns
	hfri.xs.rf <- dlist.scrubbed$HFRI
	market.xs.rf <- dlist.scrubbed$Market
	
	# Use pure risky returns instead of nominal returns
# 	hfri.xs.rf <- dlist.scrubbed$HFRI - dlist.scrubbed$Riskfree
# 	market.xs.rf <- dlist.scrubbed$Market - dlist.scrubbed$Riskfree

    # Use adjusted market index instead of HFRI
# 	market.xs.rf.adj <- ifelse(market.xs.rf > 0, market.xs.rf * 0.83, abs(market.xs.rf * 0.83) ^ (1/3) * sign(market.xs.rf))
# 	hfri.xs.rf <- ifelse(calc.rnd(hfri.xs.rf, 15) - calc.rnd(market.xs.rf, 15) > 0, hfri.xs.rf, market.xs.rf.adj)
	hfri.xs.rf <- pmax(hfri.xs.rf, market.xs.rf)
	
	size <- dlist.scrubbed$Size
	value <- dlist.scrubbed$Value
#	put <- dlist.scrubbed$PutWrite - dlist.scrubbed$Riskfree
# 	put <- dlist.scrubbed$SPPut.buywrite
#  	put <- dlist.scrubbed$SPPut
    volprem <- dlist.scrubbed$SPPut + dlist.scrubbed$Riskfree
	put <- volprem + dlist.scrubbed$SwapCost * -sign(volprem)
	em <- dlist.scrubbed$EMBias
	
	# List of tracking target (HFRI xs rf) and beta-factor returns
	dlist.indexvar <- cbind(hfri.xs.rf, market.xs.rf, size, value, put, em)
	
	market <- dlist.scrubbed[,8]
	rf <- dlist.scrubbed[,9]
	risk.market <- market - rf
	vix.market <- dlist.scrubbed[,10]
	skew.market <- dlist.scrubbed[,11]
	risk.put <- market - rf
	vix.put <- dlist.scrubbed[,10]
	skew.put <- dlist.scrubbed[,11]
	m1.chg <- dlist.scrubbed[,12]
	intercept.market <- dlist.scrubbed[,13]
	intercept.put <- dlist.scrubbed[,14]
	
	# List of conditioning variables
	dlist.econvar <- cbind(risk.market, rf, vix.market, skew.market, risk.put, vix.put, skew.put, m1.chg)
	attributes(dlist.econvar)$dimnames[[2]] <- c("Risk.Mkt","Riskfree","VIX.Mkt","Skew.Mkt","Risk.Put","VIX.Put","Skew.Put", "M1.Chg")

	# List of intercepts in the conditioning formula
	dlist.intercept <- cbind(intercept.market, intercept.put)
	attributes(dlist.intercept)$dimnames[[2]] <- c("Incpt.Mkt","Incpt.Put")

	# Risk-free rate
	dlist.riskfree <- dlist.scrubbed$Riskfree

	############################################################################
	# Set number of factors in model environment
	set.global("numBetaFactors", ncol(dlist.indexvar) - 1)
	
	# Set number of conditioning variables in model environment
	set.global("numConditioningVars", ncol(dlist.econvar))
	
	# Set number of conditioning variables in model environment
	set.global("numConditioningInt", ncol(dlist.intercept))

	############################################################################
	# Need to lag the conditioning variables
	# We use lagged conditioning variables to condition beta factors	
	dlist.econvar.lag1 <- Oper.lag1(dlist.econvar)
			
	############################################################################

	return(cbind(dlist.indexvar, dlist.econvar.lag1, dlist.intercept, dlist.riskfree))
}

