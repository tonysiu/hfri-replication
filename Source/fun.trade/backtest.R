backtest.startDt <- as.Date("1990-04-01")
#backtest.startDt <- as.Date("1990-08-07")
# Set backtest.endDt to end date of dataset
# backtest.endDt <- end(dlmFiltered.to.state.param.xts(get.global("obj.dlmFiltered")))
backtest.endDt <- as.Date("2012-12-31")

# Time indices
backtest.index.BoM <- seq(backtest.startDt, backtest.endDt, by="1 month")
backtest.index.EoM <- as.Date(sapply(backtest.index.BoM, get.nextMonth.from.Date))-1
backtest.index.EoM[length(backtest.index.EoM)] <- backtest.endDt
backtest.index.day <- seq(backtest.startDt, backtest.endDt, by="1 day")
backtest.index.week <- index(as.xts(backtest.index.day))[endpoints(backtest.index.day, on="weeks")]

# State vector
xts.state.vectors <- dlmFiltered.to.state.param.xts(get.global("obj.dlmFiltered"))

### Lag state vector by 2 months to reflect lagged HFRI index data
xts.state.vectors <- lapply(xts.state.vectors, function(x) lag(x, 2))

# Calculate a list of security positions by date from state vectors
xts.pos.data.historical.monthly <- calc.PosList.monthly(xts.state.vectors)
xts.pos.data.historical.daily <- calc.PosList.daily(xts.state.vectors, backtest.endDt)

# Calculate a list of security prices by date
xts.price.data.historical <- get.price.data.historical()

### Adjust daily positions based on daily conditioning variable values
xts.pos.data.historical <- adj.pos.with.daily.condvars(xts.pos.data.historical.daily, 
                                                       xts.state.vectors, 
                                                       xts.price.data.historical, 
                                                       transform.factor = 1.5,
                                                       backtest.endDt)

# Calculate historical returns based on positions determined monthly
# Monthly Rebalancing
calc.port_ret.monthly <- function() {
    # Lag x periods in trading positions
    posList <- lapply(xts.pos.data.historical[1:8], function(x) lag(x, 1))
    #posList = xts.pos.data.historical[1:8]
    priceList = xts.price.data.historical[1:8]
    priceList.libor = list("us0003m.daily" = xts.price.data.historical$us0003m.daily, 
                           "us0003m.weekly" = xts.price.data.historical$us0003m.weekly, 
                           "us0003m.monthly" = xts.price.data.historical$us0003m.monthly)
	
	# Log inputs
	logCSV.xtsList(posList, filename="posList.monthly")
	logCSV.xtsList(priceList, filename="priceList.monthly")
	
    # Remove old log file to indicate new series of calculations. port_ret.PoP starts a new
    # log file and then appends to the same file.
    if (file.exists(".\\log\\port_ret.PoP.monthly.csv")) file.remove(".\\log\\port_ret.PoP.monthly.csv")
    
    # Setting up parameters for parallel computing
#     cl <- makeCluster(4, type = "SOCK")
#     clusterEvalQ(cl, require("xts"))
#     registerDoSNOW(cl)
    
    res <- .matrix.to.xts(do.call(rbind,
                                  suppressWarnings(
                                      llply(.data = backtest.index.EoM,
                                            .fun = port_ret.PoP,
                                            interval = "monthly",
                                            posList = posList,
                                            priceList = priceList,
                                            priceList.libor = priceList.libor,
                                            BoP_pos.asOf = BoP_pos.asOf, 
                                            secur_ret.holding_period = secur_ret.holding_period, 
                                            calc.port_ret.PoP = calc.port_ret.PoP,
                                            # parallel option doesn't work for monthly r/b for some reason
                                            .parallel = F
                                      )
                                  )
    ))
#     stopCluster(cl)
    
    return(res)
}
					
# Calculate cumulative returns
calc.cum_port_ret <- function(port_ret) {
	cumprod(port_ret + 1) - 1
}

# Weekly rebalancing
calc.port_ret.weekly <- function() {
    # Lag x periods in trading positions
    posList <- lapply(xts.pos.data.historical[1:8], function(x) lag(x, 1))
    #posList = xts.pos.data.historical[1:8]
    priceList = xts.price.data.historical[1:8]
    priceList.libor = list("us0003m.daily" = xts.price.data.historical$us0003m.daily, 
                           "us0003m.weekly" = xts.price.data.historical$us0003m.weekly, 
                           "us0003m.monthly" = xts.price.data.historical$us0003m.monthly)
        
    # Log inputs
    logCSV.xtsList(posList, filename="posList.weekly")
    logCSV.xtsList(priceList, filename="priceList.weekly")
    
    # Remove old log file to indicate new series of calculations. port_ret.PoP starts a new
    # log file and then appends to the same file.
    if (file.exists(".\\log\\port_ret.PoP.weekly.csv")) file.remove(".\\log\\port_ret.PoP.weekly.csv")
    
    # Setting up parameters for parallel computing
    cl <- makeCluster(8, type = "SOCK")
    clusterEvalQ(cl, require("xts"))
    #registerDoParallel(cl)
    registerDoSNOW(cl)
    
    res <- .matrix.to.xts(do.call(rbind,
                                  suppressWarnings(
                                      llply(.data = backtest.index.week,
                                            .fun = port_ret.PoP,
                                            interval = "weekly",
                                            posList = posList,
                                            priceList = priceList,
                                            priceList.libor = priceList.libor,
                                            BoP_pos.asOf = BoP_pos.asOf, 
                                            secur_ret.holding_period = secur_ret.holding_period, 
                                            calc.port_ret.PoP = calc.port_ret.PoP,
                                            #.progress = progress_win(title="Working..."),  #doesn't work                               
                                            .parallel = T
                                      )
                                  )
    ))
    stopCluster(cl)
    
    return(res)
}

# Daily rebalancing
calc.port_ret.daily <- function() {
    # Lag x periods in trading positions
    posList <- lapply(xts.pos.data.historical[1:8], function(x) lag(x, 1))
    #posList = xts.pos.data.historical[1:8]
	priceList = xts.price.data.historical[1:8]
    priceList.libor = list("us0003m.daily" = xts.price.data.historical$us0003m.daily, 
                           "us0003m.weekly" = xts.price.data.historical$us0003m.weekly, 
                           "us0003m.monthly" = xts.price.data.historical$us0003m.monthly)
    
    # Log inputs
    posList <- assignListNametoVector(posList)
    priceList <- assignListNametoVector(priceList)
    
	logCSV.xtsList(posList, filename="posList.daily")
	logCSV.xtsList(priceList, filename="priceList.daily")
	
	# Remove old log file to indicate new series of calculations. port_ret.PoP starts a new
 	# log file and then appends to the same file.
	if (file.exists(".\\log\\port_ret.PoP.daily.csv")) file.remove(".\\log\\port_ret.PoP.daily.csv")
	
    # Setting up parameters for parallel computing
    cl <- makeCluster(8, type = "SOCK")
    clusterEvalQ(cl, require("xts"))
    #registerDoParallel(cl)
    registerDoSNOW(cl)
    
	res <- .matrix.to.xts(do.call(rbind,
	                              suppressWarnings(
        	                        llply(.data = backtest.index.day,
                                          .fun = port_ret.PoP,
                                          interval = "daily",
        	                              posList = posList,
        	                              priceList = priceList,
        	                              priceList.libor = priceList.libor,
        	                              BoP_pos.asOf = BoP_pos.asOf, 
        	                              secur_ret.holding_period = secur_ret.holding_period, 
        	                              calc.port_ret.PoP = calc.port_ret.PoP,
        	                              #.progress = progress_win(title="Working..."),  #doesn't work                               
        	                              .parallel = T
        	                        )
	                              )
	))
    stopCluster(cl)
    
    return(res)
}
