mainDir <- "./"
logDir <- "Log"

# Create log directory. Command doesn't do anything if directory is already created.
dir.create(file.path(mainDir, logDir), showWarnings = FALSE)

logCSV.xtsList <- function(xtsList, filename="log", ...) {
	df.out <- Reduce (merge, xtsList)
	write.zoo(df.out, file = paste(".\\Log\\",filename,".csv",sep=""), sep = ",", row.names = F, ...)
}

logCSV.xtsDF <- function(xtsDF, filename="log", ...) {
	write.zoo(xtsDF, file = paste(".\\Log\\",filename,".csv",sep=""), sep = ",", row.names = F, ...)
}

logCSV.regDF <- function(regDF, filename="log", ...) {
	write.table(regDF, file = paste(".\\Log\\",filename,".csv",sep=""), sep = ",", row.names = F, ...)
}

assignListNametoVector <- function(mylist) {
    listLen <- length(mylist)    
    for (i in 1:listLen) names(mylist[[i]]) <- names(mylist[i])    
    return(mylist)
}